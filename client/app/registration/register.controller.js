//TODO: Add Angular validation to your form
//TODO 4: Set Default value for gender Buttons->
//TODO 6: Set Default Dates

(function() {
    angular
        .module("EMS")
        .controller("RegCtrl", RegCtrl);
    RegCtrl.$inject = [];

    function RegCtrl() {
        var vm = this;

        // TODO 6.1: Declare today as today's date
        //TODO 6.2: Set Birthday to Today's Date 18 years ago

        vm.employee = {
            empNo: "",
            firstname: "",
            lastname: "",
            // TODO 4.2: Set default value of gender buttons to male
            gender: "",
            // TODO 6.3: Set default values of birthday and today
            birthday: "",
            hiredate:"",
            phonenumber: ""
        };

        vm.register = register;

        function register() {

            alert("The registration information you sent are \n" + JSON.stringify(vm.employee));

            console.log("The registration information you sent were:");
            console.log("Employee Number: " + vm.employee.empNo);
            console.log("Employee First Name: " + vm.employee.firstname);
            console.log("Employee Last Name: " + vm.employee.lastname);
            console.log("Employee Gender: " + vm.employee.gender);
            console.log("Employee Birthday: " + vm.employee.birthday);
            console.log("Employee Hire Date: " + vm.employee.hiredate);
            console.log("Employee Phone Number: " + vm.employee.phonenumber);
        }

    }



})();